
# 简介

利用React框架实现的一个迷宫程序，可以随机生成指定大小的“完全迷宫”，并支持解迷宫。

> 迷宫程序体验 [链接](http://static.hudk.top/maze/index.html)

## 迷宫生成效果图：

![image.png](https://b3logfile.com/file/2020/05/image-57ea0773.png)

## 解迷宫后效果图（从左上角到右下角，绿色线路是迷宫中的唯一通路）：

![image.png](https://b3logfile.com/file/2020/05/image-5beed876.png)