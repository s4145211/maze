
- 可以随机生成指定大小的“完全迷宫”，并支持解迷宫。

- 迷宫程序体验  [链接](http://demos.hudk.top/maze)，

## 迷宫生成：

![image.png](https://b3logfile.com/file/2020/05/image-57ea0773.png)

## 解迷宫：

![image.png](https://b3logfile.com/file/2020/05/image-5beed876.png)